//
//  ViewController.swift
//  3
//
//  Created by Alexandra Gorshkova on 15.11.17.
//  Copyright © 2017 Alexandra Gorshkova. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var scrolView: UIScrollView!
    @IBOutlet weak var loginView: UITextField!
    @IBOutlet weak var passwordView: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // жест нажатия
        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action:
            #selector(self.hideKeyboard))
        // присваиваем его UIScrollVIew
        scrolView?.addGestureRecognizer(hideKeyboardGesture)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Подписываемся на два уведомления, одно приходит при появлении клавиатуры
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name:
            NSNotification.Name.UIKeyboardWillShow, object: nil)
        // Второе когда она пропадает
        NotificationCenter.default.addObserver(self, selector:
            #selector(self.keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide,
                                                                 object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide,
                                                  object: nil)
    }
    
    @objc func hideKeyboard() {
        self.scrolView?.endEditing(true)
    }
    
    @IBAction func loginButtenPressed(_ sender: Any) {
        // получаем текст логина
       // let login = loginView.text!
        // получаем текст пароль
        //let password = passwordView.text!
        // проверяем верны ли они
        //if login == "admin" && password == "123456" {
          //  print("успешная авторизация")
        //} else {
          //  print("неуспешная авторизация")
        //}
    }
    
    // когда клавиатура появляется
    @objc func keyboardWasShown(notification: Notification) {
        // получаем размер клавиатуры
        let info = notification.userInfo! as NSDictionary
        let kbSize = (info.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
        // добавляем отступ внизу UIScrollView равный размеру клавиатуры
        self.scrolView?.contentInset = contentInsets
        scrolView?.scrollIndicatorInsets = contentInsets
    }
    //когда клавиатура исчезает
    @objc func keyboardWillBeHidden(notification: Notification) {
        // устанавливаем отступ внизу UIScrollView равный 0
        let contentInsets = UIEdgeInsets.zero
        scrolView?.contentInset = contentInsets
        scrolView?.scrollIndicatorInsets = contentInsets
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        // Проверяем данные
        let checkResult = checkUserData()
        // если данные неверны, покажем ошибку
        if !checkResult {
            showLoginError()
        }
        // вернем результат
        return checkResult
    }
    func checkUserData() -> Bool {
        let login = loginView.text!
        let password = passwordView.text!
        if login == "admin" && password == "123456" {
            return true
        } else {
            return false
        }
    }
    func showLoginError() {
        // Создаем контроллер
        let alter = UIAlertController(title: "Ошибка", message: "Введены не верные данные пользователя",
                                      preferredStyle: .alert)
        // Создаем кнопку для UIAlertController
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        // Добавляем кнопку на UIAlertController
        alter.addAction(action)
        // показываем UIAlertController
        present(alter, animated: true, completion: nil)
    }
    }
    


